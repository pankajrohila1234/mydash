import React from 'react';
import './App.scss';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import AccountPage from './components/AccountPage';
import GraphPage from './components/GraphPage';

function App() {
  return (
    <div className="app">
      <Router>
        <Routes>
          <Route path="/" element={<AccountPage />}/>
          <Route path="/graph-page" element={ <GraphPage />} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
