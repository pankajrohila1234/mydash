import React, { useState } from 'react';
import "./AccountPage.scss";
import Checkbox from '@mui/material/Checkbox';
import { useNavigate } from 'react-router-dom';

const label = { inputProps: { 'aria-label': 'Checkbox demo' } };

function AccountPage() {

  const navigate = useNavigate();

  const [email, setEmail] = useState('');
  const [emailError, setEmailError] = useState('');

  const [password, setPassword] = useState('');
  const [passwordError, setPasswordError] = useState('');

  const [confirmPassword, setConfirmPassword] = useState('');
  const [confirmPasswordError, setConfirmPasswordError] = useState('');

  const [name, setName] = useState('');
  const [nameError, setNameError] = useState('');

  const [phone, setPhone] = useState('');
  const [phoneError, setPhoneError] = useState('');

  const emailValidation = (e) => {
    const tempEmail = (e && e.target && e.target.value) || e;
    setEmail(tempEmail);
    // const re = /\S+@\S+\.\S+/;
    const re = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
    const isValidEmail = re.test(String(tempEmail).toLowerCase());
    if(!tempEmail){
        setEmailError('Enter a Email Address')
    } else if(!isValidEmail) {
        setEmailError('Enter a valid Email')
    }
    else {
      setEmailError("")
    }
  }

  const passwordValidation = (e) => {
    const tempPassword = (e && e.target && e.target.value) || e;
    setPassword(tempPassword);
    var re  = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$/;
    const isValidPassword = re.test(String(tempPassword).toLowerCase());
    if(!tempPassword){
        setPasswordError('Enter a password')
    } else if(!isValidPassword) {
        setPasswordError('Password must be atleast 8 characters long, contain atleast 1 special char and a number')
    }
    else {
      setPasswordError("")
    }
  }

  const confirmPasswordValidation = (e) => {
    const tempConfirmPassword = (e && e.target && e.target.value) || e;
    setConfirmPassword(tempConfirmPassword);
    const re = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$/;
    const isValidConfirmPassword = re.test(String(tempConfirmPassword).toLowerCase());
    if (tempConfirmPassword !== password) {
      setConfirmPasswordError("Password must be the same")
    }
    if(!tempConfirmPassword){
        setConfirmPasswordError('password doesnt match')
    } else if(!isValidConfirmPassword) {
        setConfirmPasswordError('Password must be the same')
    }
    else {
      setConfirmPasswordError("")
    }
  }

  const nameValidation = (e) => {
    const tempName = (e && e.target && e.target.value) || e;
    setName(tempName);
    const re = /^[a-zA-Z]{3,10}$/;
    const isValidName = re.test(String(tempName).toLowerCase());
    if(!tempName){
        setNameError('Enter a name')
    } else if(!isValidName) {
        setNameError('Name must be atleast 3 characters long')
    }
    else {
      setNameError("")
    }
  }

  const phoneValidation = (e) => {
    const tempPhone = (e && e.target && e.target.value) || e;
    setPhone(tempPhone);
    const re = /^\(?\d{3}\)?[-. ]?\d{3}[-. ]?\d{4}$/;
    const isValidPhone = re.test(String(tempPhone).toLowerCase());
    if(!tempPhone){
        setPhoneError('Enter a name')
    } else if(!isValidPhone) {
        setPhoneError('Enter a valid phone number')
    }
    else {
      setPhoneError("")
    }
  }

  const onFormSubmission = (e) => {
    e.preventDefault();
    navigate("/graph-page")
  }

  return (
    <div className="accountPage">
        <div className="accountPage__left">
          <img src="/graph.png" alt="" />
          <h4>Choose a date range</h4>
          <p>Lorem ipsum dolor sit amet, <br/>consectetur adipiscing elit adipiscing elit.</p>
        </div>

        <div className="accountPage__right">
            <h2>Create an account</h2>
            <form onSubmit={onFormSubmission}>
              <label>Your email address</label>
              <input onChange={emailValidation} required />
              <h6 className="error__message">{emailError}</h6>

              <label>Your password</label>
              <input onChange={passwordValidation} type="password" required />
              <h6 className="error__message">{passwordError}</h6>

              <label>Confirm your password</label>
              <input onChange={confirmPasswordValidation} type="password" required />
              <h6 className="error__message">{confirmPasswordError}</h6>

              <label>Your full name</label>
              <input onChange={nameValidation} required />
              <h6 className="error__message">{nameError}</h6>

              <label>Your phone number</label>
              <input onChange={phoneValidation} required />
              <h6 className="error__message">{phoneError}</h6>

              <div>
                <Checkbox {...label} />
                <h5>I read and agree Terms and Conditions</h5>
              </div>
              {/* <p>Create account</p> */}
              <input value="Create Account" type="submit" />
            </form>
        </div>
    </div>
  )
}

export default AccountPage;