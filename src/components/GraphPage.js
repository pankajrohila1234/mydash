import React from 'react'
import './GraphPage.scss';
import Chart from './Chart';
import { useNavigate } from 'react-router-dom';

function GraphPage() {
  const navigate = useNavigate();

  return (
    <div className="graphPage">
        <div style={{ backgroundColor: '#252D3A', height: "100vh"}}>
            <button className="graphPage__btn" onClick={(e) => navigate("/")}>Back</button>
            <div>
                <Chart color1='#ACB9FF' color2="#503795" />
            </div>
            {/* <div style={{ flex: 0.5 }}>
                <Chart color1="#BCFFF8" color2="#19B6A4" />
            </div> */}
        </div>
    </div>
  )
}

export default GraphPage